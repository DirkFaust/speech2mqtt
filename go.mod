module gitlab.com/DirkFaust/speech2mqtt

go 1.18

require (
	github.com/go-audio/audio v1.0.0
	github.com/go-audio/wav v1.1.0
	github.com/go-resty/resty v1.8.0
	github.com/joho/godotenv v1.4.0
	github.com/orcaman/writerseeker v0.0.0-20200621085525-1d3f536ff85e
	github.com/sirupsen/logrus v1.8.1
	gitlab.com/DirkFaust/faustility v0.0.0-20220624092906-6ff43124db9b
)

require (
	github.com/go-audio/riff v1.0.0 // indirect
	github.com/goiiot/libmqtt v0.9.6 // indirect
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/klauspost/compress v1.15.6 // indirect
	golang.org/x/crypto v0.0.0-20220622213112-05595931fe9d // indirect
	golang.org/x/net v0.0.0-20220607020251-c690dde0001d // indirect
	golang.org/x/sys v0.0.0-20220622161953-175b2fd9d664 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	nhooyr.io/websocket v1.8.7 // indirect
)
