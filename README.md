# Speech to text REST API

Uses [Silero Models](https://github.com/snakers4/silero-models) for transcribing speech to text.
This is a submodule for the [speech project](https://gitlab.com/DirkFaust/speech) - details there.
