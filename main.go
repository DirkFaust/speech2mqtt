package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"math"
	"os"
	"os/signal"
	"strings"
	"sync"
	"time"

	"github.com/go-audio/audio"
	"github.com/go-audio/wav"
	"github.com/go-resty/resty"
	"github.com/joho/godotenv"
	"github.com/orcaman/writerseeker"
	log "github.com/sirupsen/logrus"
	"gitlab.com/DirkFaust/faustility/pkg/audiocrypt"
	"gitlab.com/DirkFaust/faustility/pkg/conversion"
	"gitlab.com/DirkFaust/faustility/pkg/env"
	"gitlab.com/DirkFaust/faustility/pkg/mqtt"
)

const (
	envLogLevel          = "LOG_LEVEL"
	envMqttHost          = "MQTT_HOST"
	envMqttPort          = "MQTT_PORT"
	envAudioDeviceIndex  = "AUDIO_DEVICE_INDEX"
	envPassword          = "ENCRYPTION_PASSWORD"
	envSpeechAPIHost     = "SPEECH_API_HOST"
	envSpeechAPIPort     = "SPEECH_API_PORT"
	envMinLevelPercent   = "MIN_LEVEL_PERCENT"
	envMaxSilenceSeconds = "MAX_SILENCE_SECONDS"
	envMaxRecording      = "MAX_RECORDING"
)

var audioDeviceIndex = 0

type WakeWordMessage struct {
	Timestamp int64  `json:"timestamp"`
	Word      string `json:"word"`
}

type SpeechResponse struct {
	Text string  `json:"text"`
	Took float64 `json:"took"`
}

type StartStopMessage struct {
	Timestamp int64 `json:"timestamp"`
}

type LevelMessage struct {
	Timestamp    int64 `json:"timestamp"`
	LevelPercent int   `json:"level_percent"`
}

var woke = false
var started time.Time
var lastZero time.Time
var buffer []int16
var speechURL string
var minLevelPercent = 5
var maxSilence = 1 * time.Second
var maxRecording = 10 * time.Second

// percent
func level(pcm []int16) int {
	const max int16 = 32767

	var lastMax int16 = 0
	for _, i := range pcm {
		if i > lastMax {
			lastMax = i
		}
	}

	return int(math.Round(100.0 / float64(max) * float64(lastMax)))
}

func processWAV() (*SpeechResponse, error) {
	buf := &writerseeker.WriterSeeker{}
	writer := wav.NewEncoder(buf, 16000, 16, 1, 1)

	var conv []int = make([]int, len(buffer))
	for i, val := range buffer {
		conv[i] = int(val)
	}

	writer.Write(&audio.IntBuffer{
		Format: &audio.Format{
			NumChannels: 1,
			SampleRate:  16000,
		},
		SourceBitDepth: 16,
		Data:           conv,
	})
	writer.Close()

	client := resty.New()

	r := buf.Reader()
	var w bytes.Buffer
	_, err := io.Copy(&w, r)
	if err != nil {
		return nil, err
	}

	resp, err := client.R().
		SetFileReader("wav_file", "file.wav", bytes.NewReader(w.Bytes())).
		Post(speechURL)

	if err != nil {
		return nil, err
	}
	var response SpeechResponse
	err = json.Unmarshal(resp.Body(), &response)
	if err != nil {
		return nil, err
	}

	log.Infof("Result: '%s' in %.4fs", response.Text, response.Took)

	return &response, nil
}

func sendStartStop(client *mqtt.Client, what string) {
	msg := StartStopMessage{
		Timestamp: time.Now().Unix(),
	}
	content, err := json.Marshal(msg)
	if err != nil {
		return
	}
	client.Publish(mqtt.Message{
		Topic:   fmt.Sprintf("speech2mqtt/%s/%d", what, audioDeviceIndex),
		Payload: content,
	}, false)
}

func processMQTT(client *mqtt.Client, data mqtt.Message, decryptor *audiocrypt.AudioDecryptor) error {
	if strings.HasPrefix(data.Topic, "wakeword2mqtt") {
		// ignore the payload for now
		log.Info("Woke up for wakeword")
		woke = true
		started = time.Now()
		lastZero = started
		buffer = []int16{}
		sendStartStop(client, "start")
	}

	finished := false
	if woke && strings.HasPrefix(data.Topic, "audio2mqtt") {
		if time.Since(started) > maxRecording {
			log.Infof("Stopping for timeout")
			woke = false
			finished = true
		}
		// TODO: invent something like voice-presence detection
		if time.Since(lastZero) > maxSilence {
			log.Infof("Stopping for silence")
			woke = false
			finished = true
		}

		if finished {
			sendStartStop(client, "stop")
			result, err := processWAV()
			if err != nil {
				return err
			}
			content, err := json.Marshal(result)
			if err != nil {
				log.Error(err)
				return err
			}
			client.Publish(mqtt.Message{
				Topic:   fmt.Sprintf("speech2mqtt/transcription/%d", audioDeviceIndex),
				Payload: content,
			}, false)
		}

		pcm, err := decryptor.Decrypt(data.Payload)
		if err != nil {
			return err
		}

		// we have no streaming stt ready yet so we just buffer everything
		buffer = append(buffer, pcm...)

		level := level(pcm)
		log.Debugf("Level: %d%%", level)
		if level >= minLevelPercent {
			lastZero = time.Now()
		}

		msg := LevelMessage{
			Timestamp:    time.Now().Unix(),
			LevelPercent: level,
		}
		content, err := json.Marshal(msg)
		if err == nil {
			client.Publish(mqtt.Message{
				Topic:   fmt.Sprintf("speech2mqtt/level/%d", audioDeviceIndex),
				Payload: content,
			}, false)
		}
	}

	return nil
}

func main() {
	log.SetFormatter(&log.TextFormatter{FullTimestamp: true})
	log.SetOutput(os.Stdout)
	level, err := log.ParseLevel(env.GetOrDefault(envLogLevel, "info"))
	if err != nil {
		level = log.InfoLevel
	}
	log.SetLevel(level)

	_ = godotenv.Load()

	conversion.FromStringOrDie(env.GetOrDie(envAudioDeviceIndex), &audioDeviceIndex)
	conversion.FromStringOrDie(env.GetOrDie(envMinLevelPercent), &minLevelPercent)
	conversion.FromStringOrDie(env.GetOrDefault(envMaxSilenceSeconds, "1s"), &maxSilence)
	conversion.FromStringOrDie(env.GetOrDefault(envMaxRecording, "1s"), &maxRecording)

	speechURL = fmt.Sprintf("http://%s:%s/speech2text", env.GetOrDie(envSpeechAPIHost), env.GetOrDefault(envSpeechAPIPort, "80"))

	started = time.Now()
	lastZero = started

	client := mqtt.NewClient(
		nil,
		fmt.Sprintf("wakeword2mqtt/detection/%d", audioDeviceIndex),
		fmt.Sprintf("audio2mqtt/pcm/%d", audioDeviceIndex),
	)

	password := os.Getenv(envPassword)
	doEncrypt := (len(password) > 0)
	var decryptor audiocrypt.AudioDecryptor

	if !doEncrypt {
		log.Warn("NOT using encryption - its highly unadviced to send your permanent(!) recorded(!) audio unencrypted. Even if the MQTT is secured via TLS, EVERY client on that server can receive AND HEAR the audio.")
	} else {
		decryptor = audiocrypt.NewAudioDecryptor(password)
	}

	wg := sync.WaitGroup{}
	wg.Add(1)

	audio := client.Generate()

	go func() {
		signalCh := make(chan os.Signal, 1)
		signal.Notify(signalCh, os.Interrupt)
		defer close(signalCh)
		defer client.Close()

	receiveLoop:
		for {
			select {
			case <-signalCh:
				log.Infof("Interrupt requested")
				break receiveLoop
			case data := <-audio:
				processMQTT(&client, data, &decryptor)
				// processMQTT(&client, data, &decryptor, func(result SpeechResponse) {
				// 	content, err := json.Marshal(result)
				// 	if err != nil {
				// 		log.Error(err)
				// 		return
				// 	}
				// 	client.Publish(mqtt.Message{
				// 		Topic:   fmt.Sprintf("speech2mqtt/transcription/%d", audioDeviceIndex),
				// 		Payload: content,
				// 	}, false)
				// })
			}
		}
		wg.Done()
	}()

	wg.Wait()
	log.Info("Terminated")
}
