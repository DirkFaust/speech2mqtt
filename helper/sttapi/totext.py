#!/usr/bin/env python3

import sys
import torch
import time
from flask import Flask, json, request

api = Flask(__name__)


@api.route('/speech2text', methods=['GET', 'POST'])
def speech2text():
    if request.method == 'POST':
        started = time.time()
        file = request.files['wav_file']
        file.save(f"/tmp/speech.wav")
        started = time.time()
        text = run_transcription("/tmp/speech.wav")
        took = time.time() - started
        return json.dumps({"text": text, "took": took}), 200


def run_transcription(filename):
    batches = split_into_batches([filename], batch_size=1)
    input = prepare_model_input(read_batch(batches[0]), device=device)

    output = model(input)
    for example in output:
        return (decoder(example.cpu()))


if __name__ == '__main__':
    hello = "Speech2Text REST API provider v1.1"
    print("-" * len(hello))
    print(hello)
    print("-" * len(hello))

    if len(sys.argv) != 2:
        print("Usage: {} langauge-short.".format(sys.argv[0]))
        print("Example: \"{} de\" - possible: 'de', 'en', 'es'".format(
            sys.argv[0]))
        exit(1)

    language = sys.argv[1]

    device = torch.device('cpu')
    model, decoder, utils = torch.hub.load(
        repo_or_dir='snakers4/silero-models',
        model='silero_stt',
        language=language.lower(),
        device=device)
    (read_batch, split_into_batches, read_audio, prepare_model_input) = utils

    api.run(host="0.0.0.0", port=80)
